import sys

def main(fileName):
    # DPLL solver
    variableCount, formulas = readFile(fileName)

    # Formula에 한 부호로만 등장하는 확실한 variable 수집
    sureVariables = collectSureVariable(formulas)

    solve(variableCount, sureVariables, [], formulas)


def readFile(fileName):
    # input/output format
    # http://www.satcompetition.org/2009/format-benchmarks2009.html
    try:
        with open(fileName) as file:
            variableCount = 0
            formulaCount = 0
            formulas = []

            lines = file.readlines()
            for line in lines:
                if line.startswith('c'): continue # c (comment)

                chars = line.split(' ')
                while '' in chars: # 입력에 불필요한 공백이 많은 경우 지워냄
                    chars.remove('')

                if line.startswith('p'): # p cnf (variableCount) (formulaCount)
                    variableCount = int(chars[2])
                    formulaCount = int(chars[3])
                    continue

                if len(formulas) == formulaCount:
                    break

                # formula such as 1 -5 4 0 (means 'x1 and not x5 and x4')
                formula = [int(char) for char in chars[:-1]]
                formulas.append(formula)

            return (variableCount, formulas)
    except FileNotFoundError:
        print(fileName, "file not found")
        sys.exit(1)


def solve(variableCount, sureVariables, propositions, formulas):
    # printState(variableCount, sureVariables, propositions, formulas) # print하면 현재 상태 확인 가능

    # Formula에 unit variable만 남는 경우 수집
    satisfied, sureVariables, propositions = collectUnitVariable(sureVariables, propositions, formulas)

    # Conflict 발생 시 backtrack 또는 종료
    known = collectKnown(sureVariables, propositions)
    if checkConflict(known):
        fixed, propositions = tryFix(propositions)
        # 수습 불가능하면 종료
        if not fixed:
            print('s UNSATISFIABLE')
            return

        # 바꾼 가정으로 다시 시도
        solve(variableCount, sureVariables, propositions, formulas)
        return
    elif satisfied:
        # Satisfied이면 종료
        knownStrings = [str(variable) for variable in known]
        print('s SATISFIABLE')
        print('v ' + ' '.join(knownStrings) + ' 0')
        return

    # 적절한 가정을 하나 추가하고 다시 solve
    variable = selectProposition(known, formulas)
    propositions.append([variable, False, set()])
    solve(variableCount, sureVariables, propositions, formulas)


# Formula에 unit variable만 남는 경우 수집
def collectUnitVariable(sureVariables, propositions, formulas):
    unitVariables = set()
    satisfied = True
    known = collectKnown(sureVariables, propositions)

    for formula in formulas:
        reduced = reduceFormula(known, formula)
        length = len(reduced)

        if length > 1:
            satisfied = False

        if length == 1:
            unitVariables.add(reduced[0])

    if len(unitVariables) == 0:
        # 더 이상 unit variable만 남는 경우가 없음
        return satisfied, sureVariables, propositions

    if propositions == []:
        # 가정 없이 얻은 unit variable을 확정시킴
        sureVariables = sureVariables.union(unitVariables)
    else:
        # 가장 최근에 가정했던 variable에 새로 얻은 unit variable을 종속시킴
        # 가정했던 variable에 문제가 있는 걸로 판명되면 통째로 backtrack 할 수 있게
        [proposition, isConflicted, obtained] = propositions[-1]
        obtained = obtained.union(unitVariables)
        propositions.pop()
        propositions.append([proposition, isConflicted, obtained])

    # 새로운 unit variable이 생길 수 있으니 재확인
    return collectUnitVariable(sureVariables, propositions, formulas)


# 이미 알고 있는 variable을 제거해서 formula를 간단한 형태로 reduce
def reduceFormula(known, formula):
    reduced = []
    for variable in formula:
        if variable in known:
            # True인 variable을 가지고 있어서 formula가 항상 true
            return []
        elif -variable in known:
            # False인 variable을 가지고 있어서 제거
            continue
        else:
            # 알 수 없는 variable을 가지고 있어서 그대로 둠
            reduced.append(variable)

    return reduced


# 알고 있는 모든 variable을 정리
def collectKnown(sureVariables, propositions):
    known = set()
    known = known.union(sureVariables)

    for proposition in propositions:
        [variable, _, obtained] = proposition
        known.add(variable)
        known = known.union(obtained)

    return known


# Formula에 한 부호로만 등장하는 확실한 variable 수집
def collectSureVariable(formulas):
    variables = set()
    for formula in formulas:
        for variable in formula:
            if variable not in variables:
                # 처음 등장한 variable의 부호 저장
                variables.add(variable)

    # 한 부호만 등장한 variable을 수집
    variables = set(filter(lambda elem: -elem not in variables, variables))
    return variables


def checkConflict(variables):
    for variable in variables:
        if -variable in variables:
            return True

    return False


def tryFix(propositions):
    if propositions == []:
        # 되돌릴 수 있는 가정이 없음
        return (False, propositions)
    else:
        # 가장 최근에 가정했던 것을 취소함
        [variable, isConflicted, _] = propositions.pop()
        if isConflicted:
            # True와 False를 모두 사용해봤지만 variable에 대해서 conflict가 발생
            # 한번 더 pop해야 함
            return tryFix(propositions)
        else:
            # variable의 부호를 반대로 가정
            propositions.append([-variable, True, set()])
            return (True, propositions)


# 적절한 가정 하나를 찾음
def selectProposition(known, formulas):
    leastLength = 0
    leastReduced = []

    for formula in formulas:
        reduced = reduceFormula(known, formula)
        length = len(reduced)

        if reduced == []:
            continue

        if leastReduced == [] or leastLength > length:
            leastLength = length
            leastReduced = reduced

    assert(leastReduced != [])
    return leastReduced[0]

def printState(variableCount, sureVariables, propositions, formulas):
    print(f'print current state ... {variableCount} variables .........')
    for formula in formulas:
        print(f'formula : {formula}')
    for variable in sureVariables:
        print(f'sure variable {variable}')
    for [variable, isConflicted, obtained] in propositions:
        print(f'proposition variable {variable}, isConflicted {isConflicted}')
        for obtainedVariable in obtained:
            print(f'obtained variable {obtainedVariable}')
    print('print current state finished .........')


if __name__ == '__main__':
    sys.setrecursionlimit(3000)
    if len(sys.argv) != 2:
        print(sys.argv[0], 'invalid arguments')
        sys.exit(1)

    main(sys.argv[1])
